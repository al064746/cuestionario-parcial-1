
import java.util.Arrays;
public class Sort {
    public static int[] radix1(int[] d){//Ordenamiento Por Radix[6]
    	int[] data=d.clone();//Clonacion de Arreglo para Replicar datos
        int i, m = data[0], exp = 1, n = data.length;
        int[] b = new int[data.length];

        for (i = 1; i < n; i++)
            if (data[i] > m)
                m = data[i];

        while (m / exp > 0){
            int[] bucket = new int[data.length];

            for (i = 0; i < n; i++)
                bucket[(data[i] / exp) % data.length]++;

            for (i = 1; i < data.length; i++)
                bucket[i] += bucket[i - 1];

            for (i = n - 1; i >= 0; i--)
                b[--bucket[(data[i] / exp) % data.length]] = data[i];

            for (i = 0; i < n; i++)
                data[i] = b[i];

            exp *= data.length;        
        }
        return data;
    }  
}